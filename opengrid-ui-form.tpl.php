<div id="control-frame">

  <div class="opengrid-switch"><?php print $open_tab_title; ?></div>
  <?php print $grid_guide; ?>
  <div class="opengrid-ui-form-wrapper ui-draggable">

    <div class="opengrid-ui-form-header">
      <div class="opengrid-ul-tabs"><?php print $tabs; ?></div>
    </div>

    <div id="region-selected-classes">
      div id="<span class="ids-display"></span>"<br/>
      class="<span class="classes-display"></span>"
    </div>

    <?php print $form; ?>

    <div class="close-button"><?php print $close_button; ?></div>

  </div>
</div>

