Drupal.behaviors.opengridUi = function(context) {

//Grid background
  $('.grid-bg').toggle(function() {
    $('.container_16').each(function() {
      height = $(this).height();
      $('.aux-bg').css({'height': height});
      $(this).append('<div class="aux-bg " />');
      for (var i = 1; i <= 16; i++) {
        $('.aux-bg').append('<div class="grid_1" />');
      }
    });
    $('.container_12').each(function() {
      height = $(this).height();
      $('.aux-bg').css({'height': height});
      $(this).append('<div class="aux-bg " />');
      for (var i = 1; i <= 12; i++) {
        $('.aux-bg').append('<div class="grid_1" />');
      }
    });
    $('.container_24').each(function() {
      height = $(this).height();
      $('.aux-bg').css({'height': height});
      $(this).append('<div class="aux-bg " />');
      for (var i = 1; i <= 24; i++) {
        $('.aux-bg').append('<div class="grid_1" />');
      }
    });
  },
  function() {
    $('.aux-bg').remove();
  });
//Contorl box switch
  $('.opengrid-switch').click(function(e) {
    $(".region-select-box select").val('');
    $(".grid-select-op select").val('');
    $(".grid-select-columns select").val('');
    $('.opengrid-ui-form-wrapper').toggleClass('view-box');

    //makes the control box draggable
    var options = {
      handle: 'opengrid-ui-form-wrapper',
    };

    var height = $('.opengrid-ui-form-wrapper').height();
    topVal=e.pageY-(2*height)+"px";
    $('.opengrid-ui-form-wrapper').css({left:'50px',top:'300px'}).draggable(options);

  });

//Close button
  $('.close-button').click(function() {
    $('.opengrid-ui-form-wrapper').toggleClass('view-box');
    $('.grid-selected').removeClass('grid-selected');
  });

//Tabs control
  $('.opengrid-ul-tabs ul li a').click(function() {
    $('.opengrid-ul-tabs ul li a').removeClass('active-tab');
    $('.classes-display').html('');

    if ($(this).hasClass('region-select-tab')) {
      $(this).addClass('active-tab');
      $('.region-select-box').show();
      $('.block-select-box').hide();
    }
    if ($(this).hasClass('block-select-tab')) {
      $(this).addClass('active-tab');
      $('.region-select-box').hide();
      $('.block-select-box').show();
    }
    return false;
  });

//loads the options for regions in the select regions field
  $('.region').each(function(index) {
      var region_id = $(this).attr('id');
      if (region_id != '') {
        $("<option value=" + region_id + ">"+ region_id +"</option>").appendTo(".region-select-box select");
      }
  });

//loads the options for blocks in the select block field
  $('.block').each(function(index) {
    if (!$(this).is('.hidden') && !$(this).is('.context-block-hidden')) {
      var block_id = $(this).attr('id');
      if (block_id != '') {
        $("<option value=" + block_id + ">"+ block_id +"</option>").appendTo(".block-select-box select");
      }
    }
  });

//if the value for the grid-selected region is changed
  $(".region-select-box select").change(function() {
    var selected_region_id = $(".region-select-box select").val();
    $(".grid-select-box select").val('');
    $(".grid-select-op select").val('');
    $(".grid-select-columns select").val('');


    $('.grid-selected').each(function() {
      if ($(this).attr('id') != selected_region_id) {
        $(this).removeClass('grid-selected');
      }
    });

    $("#"+ selected_region_id).addClass('grid-selected');
    var classes = $('.grid-selected').attr('class');
    var classes = $('.grid-selected').attr('class').replace("grid-selected",'').replace("region",'');

    $('.ids-display').html(selected_region_id);
    $('.classes-display').html(classes);
  });

//if the value for the grid-selected block is changed
  $(".block-select-box select").change(function() {
    var selected_block_id = $(".block-select-box select").val();

    $(".region-select-box select").val('');
    $(".grid-select-op select").val('');
    $(".grid-select-columns select").val('');

    $('.grid-selected').each(function(){
      if ($(this).attr('id') != selected_block_id) {
        $(this).removeClass('grid-selected');
      }
    });

    $("#"+ selected_block_id).addClass('grid-selected');
    var classes = $('.grid-selected').attr('class').replace("grid-selected",'').replace("block",'');

    $('.ids-display').html(selected_block_id);
    $('.classes-display').html(classes);
  });

//if the value for the columns is changed
  $('.grid-select-columns select').change(function() {
        var selected_id = $('.grid-selected').attr('id');
        var layout = current_layout();
        var selected_grid_op = $(".grid-select-op select").val();
        var selected_grid_columns = $(".grid-select-columns select").val();

  if (selected_grid_op != 'none' && selected_grid_op != '' && selected_grid_columns != '') {
    var new_grid = selected_grid_op +'_'+ selected_grid_columns;
      $('.grid-selected').each(function() {
      //before add the new value, erase the old one
        var prefix = selected_grid_op + '_';
        var regx = new RegExp('\\b' + prefix + '.*?\\b', 'g');
        $('.grid-selected')[0].className = $('.grid-selected')[0].className.replace(regx, '');

      //if the value of columns is not empty,load the new value
        if (selected_grid_columns != '' && selected_grid_columns != 'none') {
          $('.grid-selected').addClass(new_grid);
        }
      });

      //region or block flag
      var type_flag = ($('.grid-selected').hasClass('region')) ? 'region' : 'block';

      $.get("/opengrid-ui-update-values", { 'data[]': [layout, selected_id, selected_grid_op, selected_grid_columns, type_flag]}, update_values);
  }
  var classes = $('.grid-selected').attr('class');
  var classes = $('.grid-selected').attr('class').replace("grid-selected",'').replace("block",'').replace("region",'');
  $('.classes-display').html(classes);
  });

//if the button for Add class is clicked
  $('.classes-fieldset-box input[id=edit-class-add]').click(function() {

    var selected_id = $('.grid-selected').attr('id');
    var layout = current_layout();

    var custom_class = $('.classes-fieldset-box input[type=text]').val();

    $('.grid-selected').addClass(custom_class);
    var classes = $('.grid-selected').attr('class');
    var classes = $('.grid-selected').attr('class').replace("grid-selected",'').replace("block",'').replace("region",'');
    $('.classes-display').html(classes);

    //region or block flag
    var type_flag = ($('.grid-selected').hasClass('region')) ? 'region' : 'block';

    $.get("/opengrid-ui-add-class", { 'data[]': [layout, selected_id, custom_class, type_flag]}, add_class);

    return false;
  });

//if the button for remove class is clicked
  $('.classes-fieldset-box input[id=edit-class-remove]').click(function() {

    var selected_id = $('.grid-selected').attr('id');
    var layout = current_layout();

    var custom_class = $('.classes-fieldset-box input[type=text]').val();
    $('.grid-selected').removeClass(custom_class);
    var classes = $('.grid-selected').attr('class');
    var classes = $('.grid-selected').attr('class').replace("grid-selected",'').replace("block",'').replace("region",'');
    $('.classes-display').html(classes);

    //region or block flag
    var type_flag = ($('.grid-selected').hasClass('region')) ? 'region' : 'block';

    $.get("/opengrid-ui-remove-class", { 'data[]': [layout, selected_id, custom_class, type_flag]}, remove_class);

    return false;
  });

//Save button is clicked
  $('#control-frame input[value=Save]').click(function() {
    $.get("/opengrid-ui-save-value", null, save_values);
    return false;
  });

}

function current_layout() {

  var front_page = Drupal.settings.opengrid_ui.front_page;

  var layout = $('body').hasClass('no-sidebars') ? 'no_sidebars_layout' : 'sidebar_first_layout';
  layout = ($('body').hasClass('two-sidebars') ) ? 'both_sidebars_layout' : layout;
  layout = ($('body').hasClass('sidebar_second') && !$('body').hasClass('sidebar_first')) ? 'sidebar_second_layout' : layout;

  //check if the frontpage layout is active
  layout = (front_page != '') ? front_page : layout;

  return layout;
}

//loads saved values for grid and class fields
var update_values = function(response) {}

var add_class = function(response) {
  $('.classes-fieldset-box input[type=text]').val('');
}

var remove_class = function(response) {
  $('.classes-fieldset-box input[type=text]').val('');
}
var save_values = function(response) {
  var saved_message = Drupal.settings.opengrid_ui.saved_message;
  $(".grid-buttons").ajaxStart (function () {
    $('#edit-save').addClass('ajax-loading');
  }).ajaxStop (function () {
      $('#edit-save').removeClass('ajax-loading');
  });
}
